//
//  JCMTableViewController.h
//  UITableViewController Challenge Solution
//
//  Created by Jared McFarland on 10/22/13.
//  Copyright (c) 2013 Jared McFarland. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface JCMTableViewController : UITableViewController

@end
